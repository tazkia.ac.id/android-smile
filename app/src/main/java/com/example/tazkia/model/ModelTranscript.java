package com.example.tazkia.model;

public class ModelTranscript {
    private String title;

    public ModelTranscript(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
