package com.example.tazkia.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.example.tazkia.R;
import com.example.tazkia.model.ModelTranscript;

import java.util.List;

public class ViewPagerTranscriptAdapter extends PagerAdapter {

    private List<ModelTranscript> modelTranscripts;
    private LayoutInflater layoutInflater;
    private Context context;

    public ViewPagerTranscriptAdapter(List<ModelTranscript> modelTranscripts, Context context){
        this.modelTranscripts = modelTranscripts;
        this.context = context;
    }

    @Override
    public int getCount() {
        return modelTranscripts.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view.equals(object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        layoutInflater = layoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.slide_transcript, container, false);

        TextView title;

        title = view.findViewById(R.id.title);

        title.setText(modelTranscripts.get(position).getTitle());

        container.addView(view, 0);
        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View)object);
    }
}
