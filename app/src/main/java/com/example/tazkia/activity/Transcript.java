package com.example.tazkia.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.animation.ArgbEvaluator;
import android.os.Bundle;
import android.widget.Adapter;

import com.example.tazkia.R;
import com.example.tazkia.adapter.ViewPagerTranscriptAdapter;
import com.example.tazkia.model.ModelTranscript;

import java.util.ArrayList;
import java.util.List;

public class Transcript extends AppCompatActivity {

    ViewPager viewPager;
    ViewPagerTranscriptAdapter viewPagerTranscriptAdapter;
    List<ModelTranscript> modelTranscripts;
    Integer[] colors = null;
    ArgbEvaluator argbEvaluator = new ArgbEvaluator();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transcript);

        modelTranscripts = new ArrayList<>();
        modelTranscripts.add(new ModelTranscript("Semester 1"));
        modelTranscripts.add(new ModelTranscript("Semester 2"));
        modelTranscripts.add(new ModelTranscript("Semester 3"));
        modelTranscripts.add(new ModelTranscript("Semester 4"));
        modelTranscripts.add(new ModelTranscript("Semester 5"));
        modelTranscripts.add(new ModelTranscript("Semester 6"));
        modelTranscripts.add(new ModelTranscript("Semester 7"));
        modelTranscripts.add(new ModelTranscript("Semester 8"));

        viewPagerTranscriptAdapter = new ViewPagerTranscriptAdapter(modelTranscripts, this);

        viewPager = findViewById(R.id.viewPager);
        viewPager.setAdapter(viewPagerTranscriptAdapter);
        viewPager.setPadding(50, 0, 50, 0);
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }
}