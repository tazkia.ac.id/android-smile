package com.example.tazkia.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.example.tazkia.R;
import com.example.tazkia.fragment.Akun;
import com.example.tazkia.fragment.Bantuan;
import com.example.tazkia.fragment.Beranda;
import com.example.tazkia.fragment.Finance;
import com.example.tazkia.fragment.Pesan;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainActivity extends AppCompatActivity {
    NavController navController;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setupNavigation();
    }

    private void setupNavigation() {

        navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        BottomNavigationView nav = findViewById(R.id.botnav);
        navController.popBackStack();
        nav.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                Fragment selectFragment = null;

                switch (menuItem.getItemId()){
                    case R.id.nav_beranda:
                        selectFragment = new Beranda();
                        break;

                    case R.id.nav_finance:
                        selectFragment = new Finance();
                        break;

                    case R.id.nav_bantuan:
                        selectFragment = new Bantuan();
                        break;

                    case R.id.nav_pesan:
                        selectFragment = new Pesan();
                        break;

                    case R.id.nav_akun:
                        selectFragment = new Akun();
                        break;

                }
                getSupportFragmentManager().beginTransaction().replace(R.id.nav_host_fragment, selectFragment).commit();
                return true;
            }
        });

    }


}