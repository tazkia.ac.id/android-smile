package com.example.tazkia.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.tazkia.R;
import com.example.tazkia.activity.KHSActivity;
import com.example.tazkia.activity.KRSActivity;
import com.example.tazkia.activity.KuliahActivity;
import com.example.tazkia.activity.Transcript;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;

import de.hdodenhof.circleimageview.CircleImageView;

public class Beranda extends Fragment implements View.OnClickListener {
    LinearLayout btnKrs, btnKhs, btnTranscript;
    TextView txtName;
    CircleImageView imageProfile;
    public Beranda() {
        // Required empty public constructor
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initialize();
        itemClick();
        googleAccount();
    }

    private void itemClick() {
        btnKrs.setOnClickListener(this);
        btnKhs.setOnClickListener(this);
        btnTranscript.setOnClickListener(this);
    }

    private void initialize() {
        btnKrs = getView().findViewById(R.id.BtnKrs);
        btnKhs = getView().findViewById(R.id.BtnKhs);
        btnTranscript = getView().findViewById(R.id.BtnTranscript);
        txtName = getView().findViewById(R.id.txtName);
        imageProfile = getView().findViewById(R.id.profileImage);
    }

    private void googleAccount(){
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(getActivity());
        if (account != null){
            String name = account.getDisplayName();
            Uri profileImage = account.getPhotoUrl();

            txtName.setText(name);

            if (profileImage == null){
                imageProfile.setImageResource(R.drawable.user);
            }
            Glide.with(this).load(String.valueOf(profileImage)).into(imageProfile);

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_beranda, container, false);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.BtnKrs :
                Intent intent = new Intent(this.getContext(), KRSActivity.class);
                startActivity(intent);
                break;
            case R.id.BtnKhs :
                Intent i = new Intent(this.getContext(), KHSActivity.class);
                startActivity(i);
                break;
            case R.id.BtnTranscript :
                Intent intent1 = new Intent(this.getContext(), Transcript.class);
                startActivity(intent1);
                break;
        }
    }
}