package com.example.tazkia;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tazkia.data.krs;

import java.util.ArrayList;

public class KRS_Adapter extends RecyclerView.Adapter<KRS_Adapter.ViewHolder> {
    private ArrayList<krs> krsArrayList;
    Context ctx;

    public KRS_Adapter(ArrayList<krs> krsArrayList, Context ctx) {
        this.krsArrayList = krsArrayList;
        this.ctx = ctx;
    }

    @NonNull
    @Override
    public KRS_Adapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.krs_item, parent, false);
        return new KRS_Adapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull KRS_Adapter.ViewHolder holder, int position) {
        krs krs = krsArrayList.get(position);
        holder.tv2.setText(krs.getSubject());
        holder.tv3.setText(krs.getLecture());
        holder.tv4.setText(krs.getRoom());
        holder.tv5.setText(krs.getDay());
        holder.tv6.setText(krs.getTime());
        for (int i=0; i<=3; i++){
            String number = Integer.toString(i);
            holder.tv1.setText(number);
            holder.tv7.setText("Delete");
        }
    }

    @Override
    public int getItemCount() {
        return krsArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView tv1, tv2, tv3, tv4, tv5, tv6, tv7;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tv1 = itemView.findViewById(R.id.krs_no);
            tv2 = itemView.findViewById(R.id.krs_subject);
            tv3 = itemView.findViewById(R.id.krs_lecture);
            tv4 = itemView.findViewById(R.id.krs_room);
            tv5 = itemView.findViewById(R.id.krs_day);
            tv6 = itemView.findViewById(R.id.krs_time);
            tv7 = itemView.findViewById(R.id.krs_option);
        }
    }
}
