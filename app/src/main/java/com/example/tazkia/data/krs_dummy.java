package com.example.tazkia.data;

import java.util.ArrayList;

public class krs_dummy {
    private static String[] subject = {
            "Ekonomi Syariah",
            "Ekonomi Makro",
            "Agama"
    };

    private static String[] lecture = {
            "Dr. Muhammad Amin",
            "Dr. M. Amin",
            "Dr. M. Amin"
    };

    private static String[] day = {
            "Senin",
            "Selasa",
            "Kamis"
    };

    private static String[] room = {
            "Room-1",
            "Room-2",
            "Room-3"
    };

    private static String[] time = {
            "11.00",
            "10.00",
            "08.00"
    };

    public static ArrayList<krs> getListData() {
        ArrayList<krs> list = new ArrayList<>();
        for (int position = 0; position < subject.length; position++) {
             krs krs = new krs();
            krs.setSubject(subject[position]);
            krs.setLecture(lecture[position]);
            krs.setRoom(room[position]);
            krs.setDay(day[position]);
            krs.setTime(time[position]);
            list.add(krs);
        }
        return list;
    }

}