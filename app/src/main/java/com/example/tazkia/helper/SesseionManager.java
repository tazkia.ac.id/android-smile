package com.example.tazkia.helper;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.example.tazkia.activity.LoginActivity;
import com.example.tazkia.activity.MainActivity;

import java.util.HashMap;

public class SesseionManager {
    SharedPreferences pref;
    SharedPreferences.Editor editor;

    Context context;
    int PRIVATE_MODE = 0;
    private static final String PREF_NAME = "SessionSmileTazkia";
    private static final String IS_LOGIN = "IsLoginIn";

    private static final String KEY_EMAIL = "Email";

    public SesseionManager(Context context){
        this.context = context;
        pref = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void createLoginSession(String email){
        editor.putBoolean(IS_LOGIN, true);
        editor.putString(KEY_EMAIL, email);

        editor.commit();
    }

    public void checkLogin(){
        if (this.IsLoginIn()){
            Intent intent = new Intent(context, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            context.startActivity(intent);
        }
    }

    public void logOut(){
        editor.clear();
        editor.commit();

        Intent intent = new Intent(context, LoginActivity.class);

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        context.startActivity(intent);
    }

    public HashMap<String, String> getUserDetail(){
        HashMap<String, String> user = new HashMap<String, String>();
        user.put(KEY_EMAIL, pref.getString(KEY_EMAIL, null));

        return user;
    }

    private boolean IsLoginIn(){
        return pref.getBoolean(IS_LOGIN, false);
    }
}
